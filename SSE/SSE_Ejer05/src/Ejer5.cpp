#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

void scalar_ejer5(float *v1, float *v2, float *v3, float *v4, int N)
{
  for( int i=0; i<(N-5); ++i ){
	v1[i] = v2[i]*v3[i];
	v1[i+1] = v1[i]-v4[i];
  }
}

void sse_ejer5(float *v1, float *v2, float *v3, float *v4, int N)
{
	// TO DO
}

#define VECTOR_SIZE 11

int main(void){
	printf("--Aligned--\n");
	float *v1_a,*v2_a,*v3_a,*v4_a;
	if( posix_memalign( (void**)&v1_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("posix_memalign ERROR\n");
	if( posix_memalign( (void**)&v2_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("posix_memalign ERROR\n");
	if( posix_memalign( (void**)&v3_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("posix_memalign ERROR\n");
	if( posix_memalign( (void**)&v4_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("posix_memalign ERROR\n");

	random_vector( v1_a, VECTOR_SIZE );
	random_vector( v2_a, VECTOR_SIZE );
	random_vector( v3_a, VECTOR_SIZE );
	random_vector( v4_a, VECTOR_SIZE );
	printf("V1:\t\t");print_vector( v1_a, VECTOR_SIZE );
	printf("V2:\t\t");print_vector( v2_a, VECTOR_SIZE );
	printf("V3:\t\t");print_vector( v3_a, VECTOR_SIZE );
	printf("V4:\t\t");print_vector( v4_a, VECTOR_SIZE );
	sse_ejer5( v1_a, v2_a, v3_a, v4_a, VECTOR_SIZE );
	printf("SSE-V1:\t\t"); print_vector( v1_a, VECTOR_SIZE );
	scalar_ejer5( v1_a, v2_a, v3_a, v4_a, VECTOR_SIZE );
	printf("Scalar-V1:\t"); print_vector( v1_a, VECTOR_SIZE );

	free( v1_a );
	free( v2_a );
	free( v3_a );
	free( v4_a );

	return 0;
}
