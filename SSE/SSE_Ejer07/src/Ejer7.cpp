#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

void transpose_matrix(float **V, int N){
	float _aux;
	for(int i=0; i<N; ++i ){
		for( int j=i; j<N; ++j ){
			_aux = V[i][j];
			V[i][j] = V[j][i];
			V[j][i] = _aux;
		}
	}
}

void scalar_ejer7(float **V1, float **V2, int N)
{
	for(int i=4; i<N; ++i ){
		for( int j=4; j<N; ++j ){
			V1[i][j] = V1[i][j-1];
			V2[i][j] = V2[i-1][j];
		}
	}
}

void sse_ejer7(float **V1, float **V2, int N)
{
	// TO DO
	// Note: you can use the auxiliary function 'transpose_matrix(V,N);'
}

#define VECTOR_SIZE 9

int main(void){
	printf("--Ejer7--\n");

	float **v_a, **v_a_sse, **v_a_scalar, **v_b, **v_b_sse, **v_b_scalar;

	// TO DO
	// reserve memory for the matrices v_a, v_b, v_a_sse, v_b_sse, v_a_scalar and v_b_scalar

	random_matrix( v_a, VECTOR_SIZE, VECTOR_SIZE );
	random_matrix( v_b, VECTOR_SIZE, VECTOR_SIZE );
	copy_matrix( v_a, v_a_sse, VECTOR_SIZE, VECTOR_SIZE );
	copy_matrix( v_b, v_b_sse, VECTOR_SIZE, VECTOR_SIZE );
	copy_matrix( v_a, v_a_scalar, VECTOR_SIZE, VECTOR_SIZE );
	copy_matrix( v_b, v_b_scalar, VECTOR_SIZE, VECTOR_SIZE );

	sse_ejer7( v_a_sse, v_b_sse, VECTOR_SIZE );
	scalar_ejer7( v_a_scalar, v_b_scalar, VECTOR_SIZE );

	printf("V1:\n"); print_matrix( v_a, VECTOR_SIZE, VECTOR_SIZE );
	printf("V2:\n"); print_matrix( v_b, VECTOR_SIZE, VECTOR_SIZE );

	printf("V1-SSE:\n"); print_matrix( v_a_sse, VECTOR_SIZE, VECTOR_SIZE );
	printf("V2-SSE:\n"); print_matrix( v_b_sse, VECTOR_SIZE, VECTOR_SIZE );

	printf("V1-Scalar:\n"); print_matrix( v_a_scalar, VECTOR_SIZE, VECTOR_SIZE );
	printf("V2-Scalar:\n"); print_matrix( v_b_scalar, VECTOR_SIZE, VECTOR_SIZE );

	// TO DO
	// free memory (all matrices)

	return 0;
}
