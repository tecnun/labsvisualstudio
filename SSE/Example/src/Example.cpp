
//#include <xmmintrin.h>	// SSE intrinsic
#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"
#include <math.h>
#include <Windows.h> 
#include <malloc.h> // Est� incluida en windows.h tambi�n

using namespace std;

#define VECTOR_SIZE_N4 1024
#define NUM_ITERATIONS 1


void scalar_sqrt(float *a,float*dst, int N){
	int i;
	for (i = 0; i < N; i++) {
		dst[i] = sqrt(a[i]);
	}
}

void sse_sqrt(float *a,float *dst, int N){
	// We assume N % 4 == 0.
	int nb_iters = N / 4;
	__m128 *a_ptr = (__m128*) a;
	__m128 *a_ptr2 = (__m128*) &a[0];
	__m128 a_vector1 = _mm_load_ps(a);
	__m128 a_vector2 = _mm_load_ps(&a[0]);
	__m128 *ptr = (__m128*)dst;
	int i;
	for (i = 0; i < nb_iters; i++, a_ptr++, dst += 4) {
		_mm_store_ps(dst, _mm_sqrt_ps(*a_ptr));
	}
}


int main(void){

	// Variables to analyze the performance
	long long start1 = 0, start2 = 0, end1 = 0, end2 = 0, freq = 0;

	// Reserva est�tica
#if __GNUC__ /* GCC */
	float v0_N4_f[VECTOR_SIZE_N4] __attribute__((aligned(16)));
	float vres_N4_f[VECTOR_SIZE_N4] __attribute__((aligned(16)));
#else /* visual studio */
	__declspec(align(16)) float v0_N4_f[VECTOR_SIZE_N4];// __attribute__((aligned(16)));
	__declspec(align(16)) float vres_N4_f[VECTOR_SIZE_N4];// __attribute__((aligned(16)));
#endif

	
	//Reserva din�mica: Ejemplo para reserva de Floats
#if __GNUC__ /* GCC */
	if (posix_memalign((void**)&v0_a, 16, VECTOR_SIZE * sizeof(float)) != 0)
		printf("ERROR posix_memalign\n");
	if (posix_memalign((void**)&v1_a, 16, VECTOR_SIZE * sizeof(float)) != 0)
		printf("ERROR posix_memalign\n");
	if (posix_memalign((void**)&vres_a, 16, VECTOR_SIZE * sizeof(float)) != 0)
		printf("ERROR posix_memalign\n");
#else /* visual studio */
	 // Using _aligned_malloc  Requiere la cabecera malloc.h
	float *v0_u = (float*)_aligned_malloc(VECTOR_SIZE_N4 * sizeof(float), 16);
	if (is_aligned((int*)&v0_u) != 0)
		printf("NOT aligned\n");
	else
		printf("Aligned\n");
	if (is_aligned((int*)&v0_u) != 0)
		float *v1_u = (float*)_aligned_malloc(VECTOR_SIZE_N4 * sizeof(float), 16);
	if (is_aligned((int*)&v0_u) != 0)
		printf("NOT aligned\n");
	else
		printf("Aligned\n");
	float *vres_u = (float*)_aligned_malloc(VECTOR_SIZE_N4 * sizeof(float), 16);
	if (is_aligned((int*)&v0_u) != 0)
		printf("NOT aligned\n");
	else
		printf("Aligned\n");
#endif


	print_mem_address( (int*)(&v0_N4_f[0]), 1 );
	if( !is_aligned((int*)(&v0_N4_f[0])) ) printf("unaligned memory!\n");
	print_mem_address( (int*)(&vres_N4_f[0]), 1 );
	if( !is_aligned((int*)(&vres_N4_f[0])) ) printf("unaligned memory!\n");

	random_vector( v0_N4_f, VECTOR_SIZE_N4 );
	printf("\nSSE:\t\t\n");
	print_vector(v0_N4_f,VECTOR_SIZE_N4);

	QueryPerformanceCounter((LARGE_INTEGER *)&start1);
	for (long aux=0;aux<NUM_ITERATIONS;aux++){
		sse_sqrt(v0_N4_f, vres_N4_f, VECTOR_SIZE_N4);
	}
	QueryPerformanceCounter((LARGE_INTEGER *)&end1);
	print_vector(vres_N4_f,VECTOR_SIZE_N4);

	printf("Scalar:\t\t\n");

	QueryPerformanceCounter((LARGE_INTEGER *)&start2);
	for (long aux=0;aux<NUM_ITERATIONS;aux++){
		scalar_sqrt(v0_N4_f,vres_N4_f, VECTOR_SIZE_N4);
	}
	QueryPerformanceCounter((LARGE_INTEGER *)&end2);
	print_vector(vres_N4_f,VECTOR_SIZE_N4);

	

	printf("\n\n/********* PERFORMANCE: **********/");
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	printf("\nSSE:  %d\t\t", (end1 - start1));
	printf("\nScalar:  %d\t\t", (end2 - start2));
	printf("\nSpeedUp=%f", (float)((float)(end2 - start2) / (float)(end1 - start1)));
	printf("\nQueryPerformanceCounter minimum resolution: 1/%ld Seconds.\n", freq);



	system("pause");

	return 0;
}
