#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

void scalar_ejer8(float *V, int N)
{
	for(int i=0; i<N; ++i){
		if(V[i]>0.f)
			V[i] = V[i] + 1.f;
	}
}

void sse_ejer8(float *V, int N)
{
	// TO DO
}

#define VECTOR_SIZE 9

int main(void){
	printf("--Ejer8--\n");

	float *v_a, *v_a_sse, *v_a_scalar;
	if( posix_memalign( (void**)&v_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("posix_memalign ERROR\n");
	if( posix_memalign( (void**)&v_a_sse, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("posix_memalign ERROR\n");
	if( posix_memalign( (void**)&v_a_scalar, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
			printf("posix_memalign ERROR\n");

	random_vector( v_a, VECTOR_SIZE );
	copy_vector( v_a, v_a_sse, VECTOR_SIZE );
	copy_vector( v_a, v_a_scalar, VECTOR_SIZE );

	sse_ejer8( v_a_sse, VECTOR_SIZE );
	scalar_ejer8( v_a_scalar, VECTOR_SIZE );

	printf("V:\n"); print_vector( v_a, VECTOR_SIZE );
	printf("V-SSE:\n"); print_vector( v_a_sse, VECTOR_SIZE );
	printf("V-Scalar:\n"); print_vector( v_a_scalar, VECTOR_SIZE );

	free( v_a );
	free( v_a_sse );
	free( v_a_scalar );

	return 0;
}
