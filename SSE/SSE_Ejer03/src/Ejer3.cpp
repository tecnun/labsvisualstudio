#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

float scalar_ejer3(float* a, float *b, float *dst, int N)
{
  float suma =0.f;
  for( int i=0; i<N; ++i ){
	  suma =  a[i] * b[i];
	  dst[i] = suma + suma;
  }
  return suma;
}

float sse_ejer3(float* a, float *b, float *dst, int N)
{
	// TO DO
	// a, b and dst are aligned
}

#define VECTOR_SIZE 11

int main(void){
	printf("--Aligned--\n");
	float *v1_a,*v2_a,*v3_a, suma;

	// TO DO
	// Reserve memory (posix_memalign)

	random_vector( v1_a, VECTOR_SIZE );
	random_vector( v2_a, VECTOR_SIZE );
	printf("V1:\t\t");print_vector( v1_a, VECTOR_SIZE );
	printf("V2:\t\t");print_vector( v2_a, VECTOR_SIZE );
	suma = sse_ejer3( v1_a, v2_a, v3_a, VECTOR_SIZE );
	printf("SSE-V3:\t\t"); print_vector( v3_a, VECTOR_SIZE );
	printf("SSE-suma:\t\t"); printf("%2.2f\n",suma);
	suma = scalar_ejer3( v1_a, v2_a, v3_a, VECTOR_SIZE );
	printf("Scalar-V3:\t\t"); print_vector( v3_a, VECTOR_SIZE );
	printf("Scalar-suma:\t\t"); printf("%2.2f\n",suma);

	// TO DO
	// free memory

	return 0;
}
