#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

void scalar_ejer4(float* a, int N)
{
  for( int i=4; i<N; ++i ){
	  a[i] = a[i-4] + 1;
  }
}

void sse_ejer4(float* a, int N)
{
	// TO DO
}

int main(void){
	// TO DO
	// 1.- Reserve memory
	// 2.- Initialize array (random)
	// 3.- Print initial values
	// 4.- call SSE function
	// 5.- Print SSE results
	// 6.- Call scalar function
	// 7.- Print scalar results
	// 8.- Free memory

	return 0;
}
