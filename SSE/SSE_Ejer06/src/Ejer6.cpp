#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

void transpose_matrix(float **V, int N){
	float _aux;
	for(int i=0; i<N; ++i ){
		for( int j=i; j<N; ++j ){
			_aux = V[i][j];
			V[i][j] = V[j][i];
			V[j][i] = _aux;
		}
	}
}

void scalar_ejer6(float **V, int N)
{
	for(int i=0; i<N; ++i ){
		for( int j=1; j<N; ++j ){
			V[i][j] = sqrt(V[i][j-1]) + 3.f;
		}
	}
}

void sse_ejer6(float **V, int N)
{
	// TO DO
	// Note: you can use the auxiliary function 'transpose_matrix(V,N);'
}

#define VECTOR_SIZE 5

int main(void){
	printf("--Ejer6--\n");

	float **v_a, **v_sse_copy, **v_scalar_copy;
	if( posix_memalign( (void**)&v_a, 16, VECTOR_SIZE*sizeof(float*) ) != 0 )
		printf("posix_memalign ERROR\n");
	if( posix_memalign( (void**)&v_sse_copy, 16, VECTOR_SIZE*sizeof(float*) ) != 0 )
		printf("posix_memalign ERROR\n");
	if( posix_memalign( (void**)&v_scalar_copy, 16, VECTOR_SIZE*sizeof(float*) ) != 0 )
			printf("posix_memalign ERROR\n");

	for(int i=0; i<VECTOR_SIZE; ++i ){
		if( posix_memalign( (void**)&v_a[i], 16, VECTOR_SIZE*sizeof(float) ) != 0 )
			printf("posix_memalign ERROR\n");
		if( posix_memalign( (void**)&v_sse_copy[i], 16, VECTOR_SIZE*sizeof(float) ) != 0 )
			printf("posix_memalign ERROR\n");
		if( posix_memalign( (void**)&v_scalar_copy[i], 16, VECTOR_SIZE*sizeof(float) ) != 0 )
					printf("posix_memalign ERROR\n");
	}
	random_matrix( v_a, VECTOR_SIZE, VECTOR_SIZE );
	copy_matrix( v_a, v_sse_copy, VECTOR_SIZE, VECTOR_SIZE );
	copy_matrix( v_a, v_scalar_copy, VECTOR_SIZE, VECTOR_SIZE );

	sse_ejer6( v_sse_copy, VECTOR_SIZE );
	scalar_ejer6( v_scalar_copy, VECTOR_SIZE );

	printf("V:\n");
	print_matrix( v_a, VECTOR_SIZE, VECTOR_SIZE );

	printf("V-SSE:\n");
	print_matrix( v_sse_copy, VECTOR_SIZE, VECTOR_SIZE );

	printf("V-Scalar:\n");
	print_matrix( v_scalar_copy, VECTOR_SIZE, VECTOR_SIZE );

	for(int i=0; i<VECTOR_SIZE; ++i ){
		free( v_a[i] );
		free( v_sse_copy[i] );
		free( v_scalar_copy[i] );
	}
	free( v_a );
	free( v_sse_copy );
	free( v_scalar_copy );

	return 0;
}
