#pragma once

#include <stdio.h>
#include <stdlib.h>				// For rand()...

void print_vector(const int *v, unsigned int n);

void print_vector(const float *v, unsigned int n);

void random_vector(int *v, int n);

void random_vector(float *v, int n);

void copy_vector(float *src, float *dst, int n);

void print_matrix(float **v, unsigned int Nrow, unsigned int Ncol);

void copy_matrix(float **src, float **dst, unsigned int Nrow, unsigned int Ncol);

void random_matrix(float **v, unsigned int Nrow, unsigned int Ncol);

void print_mem_address(int *v, int n);

bool is_aligned(int *mem_address);

/*void clear_vector_cache(int *v, int n);*/

/*void clear_vector_cache(float *v, int n);*/
