#include "utils.h"

void print_vector(const int *v, unsigned int n) {
	for (unsigned int i = 0; i<n; ++i)
		printf("%d ", v[i]);
	printf("\n");
}

void print_vector(const float *v, unsigned int n) {
	for (unsigned int i = 0; i<n; ++i)
		printf("%2.2f ", v[i]);
	printf("\n");
}

void random_vector(int *v, int n) {
	for (int i = 0; i<n; ++i)
		v[i] = (int)(rand() / float(RAND_MAX) * 1000);
}

void random_vector(float *v, int n) {
	for (int i = 0; i<n; ++i)
		v[i] = (rand() / float(RAND_MAX) * 1000);
}

void copy_vector(float *src, float *dst, int n) {
	for (int i = 0; i<n; ++i)
		dst[i] = src[i];
}

void print_matrix(float **v, unsigned int Nrow, unsigned int Ncol) {
	for (unsigned int i = 0; i<Nrow; ++i) {
		print_vector(v[i], Ncol);
	}
}

void copy_matrix(float **src, float **dst, unsigned int Nrow, unsigned int Ncol) {
	for (unsigned int i = 0; i<Nrow; ++i) {
		for (unsigned int j = 0; j<Ncol; ++j) {
			dst[i][j] = src[i][j];
		}
	}
}

void random_matrix(float **v, unsigned int Nrow, unsigned int Ncol) {
	for (unsigned int i = 0; i<Nrow; ++i) {
		random_vector(v[i], Ncol);
	}
}

void print_mem_address(int *v, int n) {
	printf("Hex: ");
	for (int i = 0; i<n; ++i)
		printf("%p ", &v[i]);
	printf("/ Dec: ");
	for (int i = 0; i<n; ++i)
		printf("%u ", (unsigned int)(&v[i]));
}

bool is_aligned(int *mem_address) {
	if (((unsigned int)mem_address) & 0xF) return false;
	return true;
}

/*void clear_vector_cache(int *v, int n){
sync();
for(int i=0; i<n; ++i )
asm volatile ("clflush (%0)" :: "r"(&v[i]));
//_mm_clflush( &v[i] );
sync();
}*/

/*void clear_vector_cache(float *v, int n){
sync();
for(int i=0; i<n; ++i )
asm volatile ("clflush (%0)" :: "r"(&v[i]));
//_mm_clflush( &v[i] );
sync();
}*/