#include <emmintrin.h>          // SSE2 intrinsic
#include <Windows.h>
#include "Utils.h"
#include <malloc.h>


#define VECTORIAL_REGISTER_SIZE 128
#define VECTOR_SIZE_N4 1024
#define VECTOR_SIZE_N 1023

using namespace std;






void sum_scalar(int* a, int *b, int *dst, int N)
{
	for (int i = 0; i < N; ++i) {
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
		dst[i] = a[i] + b[i];
	}
}

void sum_sse_N4(int* a, int *b, int *dst, int N)
{

	// Supuesto N % 4 == 0.

	// Obtengo el valor de Lmax para realizar Strip-Mining
	int Lmax = VECTORIAL_REGISTER_SIZE / (sizeof(int) * 8);
	// El resultado es 4. Un int ocupa 4 bytes (32bits), es decir caben 4 elementos de tipo int en cada registro vectorial

	// Calculamos el número de iteraciones que podemos hacer de manera vectorial
	int n_ite = N / Lmax;

	// Declaramos que vamos a utilizar variables del tipo registro vectorial de integer (__m128i)
	__m128i var_a, var_b, var_dst;

	// Variable para movernos realizando Strip-Mining
	int index = 0;

	// Bucle para realizar Strip-Mining
	for (int i = 0; i<n_ite; i++, a += 4, b += 4, dst += 4) {

		// Cargo desde la zona de memoria del puntero de a en el registro vectorial var_a
		var_a = _mm_load_si128((__m128i*) a); // El casting o conversión es necesaria por el analisis sintáctico del compilador

											  // Cargo desde la zona de memoria del puntero b en el registro vectorial var_b
		var_b = _mm_load_si128((__m128i*) b);// El casting o conversión es necesaria por el analisis sintáctico del compilador

											 // Realiza la operación vectorial de suma de integers de los registros vectoriales asociados a var_a y var_b
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);
		var_dst = _mm_add_epi32(var_a, var_b);

		// Asigno el resultado de la suma vectorial guardada en el registro asociado a var_dst a donde apunta dst[index]
		_mm_store_si128((__m128i*) &dst[index], var_dst);
	}

}


void sum_sse(int* a, int *b, int *dst, int N)
{
	// Supuesto N % 4 != 0

	// Obtengo el valor de Lmax para realizar Strip-Mining
	int Lmax = VECTORIAL_REGISTER_SIZE / (sizeof(int) * 8);
	// El resultado es 4. Un int ocupa 4 bytes (32bits), es decir caben 4 elementos de tipo int en cada registro vectorial

	// Calculamos el número de iteraciones que podemos hacer de manera vectorial
	int n_ite = N / Lmax;

	// Declaramos que vamos a utilizar variables del tipo registro vectorial de integer (__m128i)
	__m128i var_a, var_b, var_dst;

	// Variable para movernos realizando Strip-Mining
	int index = 0;

	// Bucle para realizar Strip-Mining
	for (int i = 0; i<n_ite; i++, index += 4) {

		// Cargo desde la zona de memoria donde apunta a[index] en el registro vectorial var_a
		var_a = _mm_load_si128((__m128i*) &a[index]);

		// Cargo desde la zona de memoria donde apunta b[index] en el registro vectorial var_b
		var_b = _mm_load_si128((__m128i*) &b[index]);

		// Realiza la operación vectorial de suma de integers de los registros vectoriales asociados a var_a y var_b
		var_dst = _mm_add_epi32(var_a, var_b);

		// Asigno el resultado de la suma vectorial guardada en el registro asociado a var_dst a donde apunta dst[index]
		_mm_store_si128((__m128i*) &dst[index], var_dst);
	}

	// Cálculo el valor del primer registro a realizar de manera escalar
	int last_item = n_ite * 4;

	// Realizo los últimos elementos de manera escalar
	for (int i = last_item; i < N; ++i) {
		dst[i] = a[i] + b[i];
	}
}


int main(void) {

	//To analyze the performance
	long long start1 = 0, start2 = 0, end1 = 0, end2 = 0, freq = 0;


//	printf("--N/4=0--\n");

	// Reserva estática
#if __GNUC__ /* GCC */
	int v0_N4[VECTOR_SIZE_N4] __attribute__((aligned(16)));
	int v1_N4[VECTOR_SIZE_N4] __attribute__((aligned(16)));
	int vres_N4[VECTOR_SIZE_N4] __attribute__((aligned(16)));
#else /* visual studio */
	__declspec(align(16)) int v0_N4[VECTOR_SIZE_N4];// __attribute__((aligned(16)));
	__declspec(align(16)) int v1_N4[VECTOR_SIZE_N4];// __attribute__((aligned(16)));
	__declspec(align(16)) int vres_N4[VECTOR_SIZE_N4];// __attribute__((aligned(16)));
#endif


	//Reserva dinámica: Ejemplo para reserva de Floats

#if __GNUC__ /* GCC */
	if( posix_memalign( (void**)&v0_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
	printf("ERROR posix_memalign\n");
	if (posix_memalign((void**)&v1_a, 16, VECTOR_SIZE * sizeof(float)) != 0)
		printf("ERROR posix_memalign\n");
	if (posix_memalign((void**)&vres_a, 16, VECTOR_SIZE * sizeof(float)) != 0)
		printf("ERROR posix_memalign\n");
#else /* visual studio */
	// Using _aligned_malloc  Requiere la cabecera malloc.h
	float *v0_u = (float*) _aligned_malloc(VECTOR_SIZE_N4 * sizeof(float), 16); 
	if (is_aligned((int*)&v0_u) != 0)
		printf("NOT aligned\n");
	else
		printf("Aligned\n");
	if (is_aligned((int*)&v0_u) != 0)
	float *v1_u = (float*)_aligned_malloc(VECTOR_SIZE_N4 * sizeof(float), 16);
	if (is_aligned((int*)&v0_u) != 0)
		printf("NOT aligned\n");
	else
		printf("Aligned\n");
	float *vres_u = (float*)_aligned_malloc(VECTOR_SIZE_N4 * sizeof(float), 16);
	if (is_aligned((int*)&v0_u) != 0)
		printf("NOT aligned\n");
	else
		printf("Aligned\n");
#endif


	random_vector(v0_N4, VECTOR_SIZE_N4);
	random_vector(v1_N4, VECTOR_SIZE_N4);
//	printf("V1:\t\t"); print_vector(v0_N4, VECTOR_SIZE_N4);
//	printf("V2:\t\t"); print_vector(v1_N4, VECTOR_SIZE_N4);
//	printf("SSE:\t\t");

	QueryPerformanceCounter((LARGE_INTEGER *)&start1);
	sum_sse_N4(v0_N4, v1_N4, vres_N4, VECTOR_SIZE_N4);
	QueryPerformanceCounter((LARGE_INTEGER *)&end1);
//	print_vector(vres_N4, VECTOR_SIZE_N4);
//	printf("Scalar:\t\t");


	QueryPerformanceCounter((LARGE_INTEGER *)&start2);
	sum_scalar(v0_N4, v1_N4, vres_N4, VECTOR_SIZE_N4);
	QueryPerformanceCounter((LARGE_INTEGER *)&end2);
//	print_vector(vres_N4, VECTOR_SIZE_N4);

	///////////////////////////////////////////////////////

//	printf("--N/4!=0--\n");


#if __GNUC__ /* GCC */
	int v0_N[VECTOR_SIZE_N4] __attribute__((aligned(16)));
	int v1_N[VECTOR_SIZE_N4] __attribute__((aligned(16)));
	int vres_N[VECTOR_SIZE_N4] __attribute__((aligned(16)));
#else /* visual studio */
	__declspec(align(16)) int v0_N[VECTOR_SIZE_N];//__attribute__((aligned(16)));
	__declspec(align(16)) int v1_N[VECTOR_SIZE_N];// __attribute__((aligned(16)));
	__declspec(align(16)) int vres_N[VECTOR_SIZE_N];// __attribute__((aligned(16)));
#endif

	random_vector(v0_N, VECTOR_SIZE_N);
	random_vector(v1_N, VECTOR_SIZE_N);
//	printf("V1:\t\t"); print_vector(v0_N, VECTOR_SIZE_N);
//	printf("V2:\t\t"); print_vector(v1_N, VECTOR_SIZE_N);
//	printf("SSE:\t\t");
	sum_sse(v0_N, v1_N, vres_N, VECTOR_SIZE_N);
	//print_vector(vres_N, VECTOR_SIZE_N);
//	printf("Scalar:\t\t");
	sum_scalar(v0_N, v1_N, vres_N, VECTOR_SIZE_N);
//	print_vector(vres_N, VECTOR_SIZE_N);


	printf("\n\n/********* PERFORMANCE: **********/");
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	printf("\nSSE:  %d\t\t", (end1 - start1));
	printf("\nScalar:  %d\t\t", (end2 - start2));
	printf("\nSpeedUp=%f", (float)((float)(end2 - start2) / (float)(end1 - start1)));
	printf("\nQueryPerformanceCounter minimum resolution: 1/%ld Seconds.\n", freq);



	system("pause");
	return 0;
}

