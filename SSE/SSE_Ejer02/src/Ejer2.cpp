#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

void mul_scalar(float* a, float *b, float *dst, int N)
{
  for( int i=0; i<N; ++i )
	  dst[i] = a[i] * b[i];
}

void mul_sse_u(float* a, float *b, float *dst, int N)
{
	// TO DO
	// a, b and c are unaligned
}

void mul_sse_a(float* a, float *b, float *dst, int N)
{
	// TO DO
	// a, b and c are aligned
}

#define VECTOR_SIZE 9

int main(void){
	printf("--Unaligned--\n");
	float *v0_u = new float[VECTOR_SIZE];
	float *v1_u = new float[VECTOR_SIZE];
	float *vres_u = new float[VECTOR_SIZE];

	print_mem_address( (int*)(&v0_u[0]), 1 );
	if( !is_aligned((int*)(&v0_u[0])) ) printf("unaligned memory!\n");
	else printf("\n");
	print_mem_address( (int*)(&v1_u[0]), 1 );
	if( !is_aligned((int*)(&v1_u[0])) ) printf("unaligned memory!\n");
	else printf("\n");
	print_mem_address( (int*)(&vres_u[0]), 1 );
	if( !is_aligned((int*)(&vres_u[0])) ) printf("unaligned memory!\n");
	else printf("\n");

	random_vector( v0_u, VECTOR_SIZE );
	random_vector( v1_u, VECTOR_SIZE );
	printf("V1:\t\t");print_vector( v0_u, VECTOR_SIZE );
	printf("V2:\t\t");print_vector( v1_u, VECTOR_SIZE );
	mul_sse_u( v0_u, v1_u, vres_u, VECTOR_SIZE );
	printf("SSE:\t\t"); print_vector( vres_u, VECTOR_SIZE );
	mul_scalar( v0_u, v1_u, vres_u, VECTOR_SIZE );
	printf("Scalar:\t\t"); print_vector( vres_u, VECTOR_SIZE );

	delete [] v0_u;
	delete [] v1_u;
	delete [] vres_u;

	//////////////////////////////////////////////

	printf("--Aligned--\n");
	float *v0_a,*v1_a,*vres_a;
	if( posix_memalign( (void**)&v0_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("ERROR posix_memalign\n");
	if( posix_memalign( (void**)&v1_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("ERROR posix_memalign\n");
	if( posix_memalign( (void**)&vres_a, 16, VECTOR_SIZE*sizeof(float) ) != 0 )
		printf("ERROR posix_memalign\n");

	print_mem_address( (int*)(&v0_a[0]), 1 );
	if( !is_aligned((int*)(&v0_a[0])) ) printf("unaligned memory!\n");
	else printf("\n");
	print_mem_address( (int*)(&v1_a[0]), 1 );
	if( !is_aligned((int*)(&v1_a[0])) ) printf("unaligned memory!\n");
	else printf("\n");
	print_mem_address( (int*)(&vres_a[0]), 1 );
	if( !is_aligned((int*)(&vres_a[0])) ) printf("unaligned memory!\n");
	else printf("\n");

	random_vector( v0_a, VECTOR_SIZE );
	random_vector( v1_a, VECTOR_SIZE );
	printf("V1:\t\t");print_vector( v0_a, VECTOR_SIZE );
	printf("V2:\t\t");print_vector( v1_a, VECTOR_SIZE );
	mul_sse_a( v0_a, v1_a, vres_a, VECTOR_SIZE );
	printf("SSE:\t\t"); print_vector( vres_a, VECTOR_SIZE );
	mul_scalar( v0_a, v1_a, vres_a, VECTOR_SIZE );
	printf("Scalar:\t\t"); print_vector( vres_a, VECTOR_SIZE );

	free( v0_u );
	free( v1_u );
	free( vres_u );

	return 0;
}
