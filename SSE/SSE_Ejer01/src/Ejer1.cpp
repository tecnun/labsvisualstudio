#include <emmintrin.h>		// SSE2 intrinsic
#include "utils.h"

using namespace std;

void sum_scalar(int* a, int *b, int *dst, int N)
{
  for (int i = 0; i < N; ++i)
    dst[i] = a[i] + b[i];
}

void sum_sse_N4(int* a, int *b, int *dst, int N)
{
	// TO DO
	// We assume N % 4 == 0.
}

void sum_sse(int* a, int *b, int *dst, int N)
{
	// TO DO
}

#define VECTOR_SIZE_N4 8
#define VECTOR_SIZE_N 9

int main2(void){
	printf("--N/4=0--\n");
	int v0_N4[VECTOR_SIZE_N4] __attribute__((aligned(16)));
	int v1_N4[VECTOR_SIZE_N4] __attribute__((aligned(16)));
	int vres_N4[VECTOR_SIZE_N4] __attribute__((aligned(16)));

	random_vector( v0_N4, VECTOR_SIZE_N4 );
	random_vector( v1_N4, VECTOR_SIZE_N4 );
	printf("V1:\t\t"); print_vector( v0_N4, VECTOR_SIZE_N4 );
	printf("V2:\t\t"); print_vector( v1_N4, VECTOR_SIZE_N4 );
	printf("SSE:\t\t");
	sum_sse_N4( v0_N4, v1_N4, vres_N4, VECTOR_SIZE_N4 );
	print_vector( vres_N4, VECTOR_SIZE_N4 );
	printf("Scalar:\t\t");
	sum_scalar( v0_N4, v1_N4, vres_N4, VECTOR_SIZE_N4 );
	print_vector( vres_N4, VECTOR_SIZE_N4 );

	///////////////////////////////////////////////////////

	printf("--N/4!=0--\n");
	int v0_N[VECTOR_SIZE_N] __attribute__((aligned(16)));
	int v1_N[VECTOR_SIZE_N] __attribute__((aligned(16)));
	int vres_N[VECTOR_SIZE_N] __attribute__((aligned(16)));

	random_vector( v0_N, VECTOR_SIZE_N );
	random_vector( v1_N, VECTOR_SIZE_N );
	printf("V1:\t\t"); print_vector( v0_N, VECTOR_SIZE_N );
	printf("V2:\t\t"); print_vector( v1_N, VECTOR_SIZE_N );
	printf("SSE:\t\t");
	sum_sse( v0_N, v1_N, vres_N, VECTOR_SIZE_N );
	print_vector( vres_N, VECTOR_SIZE_N );
	printf("Scalar:\t\t");
	sum_scalar( v0_N, v1_N, vres_N, VECTOR_SIZE_N );
	print_vector( vres_N, VECTOR_SIZE_N );

	return 0;
}
